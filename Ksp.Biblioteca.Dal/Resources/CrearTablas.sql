CREATE TABLE IF NOT EXISTS [CatalogoGenerico] (
	[IdCatalogoGenerico] integer NOT NULL PRIMARY KEY AUTOINCREMENT, 
	[Clave] varchar(20), 
	[Descripcion] varchar(100),
	[TipoDeCatalogo] varchar(25)
);

CREATE TABLE IF NOT EXISTS [Autor] (
	[IdAutor] integer NOT NULL PRIMARY KEY AUTOINCREMENT, 
	[Nombre] varchar(150), 
	[Nacionalidad] varchar(50),
	[FechaDeNacimiento] text
);

CREATE TABLE IF NOT EXISTS [Libro] (
	[IdLibro] integer NOT NULL PRIMARY KEY AUTOINCREMENT, 
	[Titulo] varchar(50), 
	[Editorial] varchar(50),
	[Anio] integer,
	[IdTipoDeLibro] integer,
	[IdAutor] integer,
	FOREIGN KEY(IdAutor) REFERENCES Autor(IdAutor)
);


CREATE TABLE IF NOT EXISTS [Copia] (
	[IdCopia] integer NOT NULL PRIMARY KEY AUTOINCREMENT, 	
	[IdLibro] integer,
	[IdEstadoDeCopia] integer,	
	FOREIGN KEY(IdLibro) REFERENCES Libro(IdLibro)
);



CREATE TABLE IF NOT EXISTS [Lector] (
	[IdLector] integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	Nombre varchar(150),
	Telefono varchar(25),
	Direccion varchar(100) 
);

CREATE TABLE IF NOT EXISTS [Multa] (
	[IdMulta] integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	[Inicio] integer,
	[Fin] integer,
	[IdLector] integer,
	FOREIGN KEY(IdLector) REFERENCES Lector(IdLector)
);

CREATE TABLE IF NOT EXISTS [Prestamo] (
	[IdPrestamo] integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	[Inicio]  integer,
	[Fin] integer,
	[IdCopia] integer,
	[IdLector] integer,
	FOREIGN KEY(IdCopia) REFERENCES Copia(IdCopia),
	FOREIGN KEY(IdLector) REFERENCES Lector(IdLector)
);

