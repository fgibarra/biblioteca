﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Ksp.Biblioteca.Bo;
using Ksp.Biblioteca.Dal.Core;
using Ksp.Biblioteca.Dal.Database;
using Ksp.Biblioteca.Dal.Entities;

namespace Ksp.Biblioteca.Dal.Repositories
{
    public class RepositoryLibros : Repository<Libro>, IRepositoryLibros
    {
        public RepositoryLibros(BibliotecaContext context) : base(context)
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Libro, BoLibro>();
                cfg.CreateMap<BoLibro, Libro>();
            });
        }

        public IEnumerable<BoLibro> All()
        {
            try
            {
                var entities = Queryable().ToList();

                return Mapper
                      .Map<IEnumerable<Libro>, IEnumerable<BoLibro>>(entities);
            }
            catch (Exception)
            {
                return new List<BoLibro>();
            }
        }

        public int InsertOrUpdate(BoLibro libro)
        {
            var entidad = Mapper
                .Map<BoLibro, Libro>(libro);

            if (entidad.IdLibro == 0)
            {
                Add(entidad);
            }
            else
            {
                Update(entidad);

            }

            return entidad.IdLibro;
        }

        public async Task<bool> Delete(int idLibro)
        {
            await Remove(idLibro);

            return true;
        }
    }
}
