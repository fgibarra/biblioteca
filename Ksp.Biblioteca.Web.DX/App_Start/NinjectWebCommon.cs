using Ksp.Biblioteca.Bll;
using Ksp.Biblioteca.Bll.Core;
using Ksp.Biblioteca.Bll.Services;
using Ksp.Biblioteca.Dal.Core;
using Ksp.Biblioteca.Dal.Repositories;
using Ksp.Biblioteca.Dal.UnitsOfWork;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Ksp.Biblioteca.Web.DX.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(Ksp.Biblioteca.Web.DX.App_Start.NinjectWebCommon), "Stop")]

namespace Ksp.Biblioteca.Web.DX.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            /*/*BindToXML(HttpContext.Current.Server.MapPath("~/App_Data/SideMenu.xml"), "/menu/*").GetHtml()*/


            kernel.Bind<ICatalogoGenericoService>().To<CatalogoGenericoService>();
            kernel.Bind<IRepositoryCatalogoGenerico>().To<RepositoryCatalogoGenerico>();
            kernel.Bind<IUnitOfCatalogos>().To<UnitOfCatalogos>();

            //Bilioteca

            //Autores
            kernel.Bind<IRepositoryAutores>().To<RepositoryAutores>();


            kernel.Bind<IBibliotecaService>().To<BibliotecaService>();
            kernel.Bind<IUnitOfBiblioteca>().To<UnitOfBiblioteca>();

        }        
    }
}
