﻿using System.Collections.Generic;
using Ksp.Biblioteca.Bo;
using Ksp.Biblioteca.Bo.Helpers;

namespace Ksp.Biblioteca.Bll.Core
{
    public interface ICatalogoGenericoService
    {
        IEnumerable<BoCatalogoGenerico> GetAll(string tipoDeCatalogo);
        IEnumerable<ValueDescription> GetTiposDeCatalogo();

        void InsertOrUpdateItemCatalogoGenerico(BoCatalogoGenerico item);

        void DeleteItemCatalogoGenerico(int idCatalogoGenerico);
    }
}
