﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Ksp.Biblioteca.Bo;
using Ksp.Biblioteca.Dal.Entities;

namespace Ksp.Biblioteca.Dal.Core
{
    public interface IRepositoryCatalogoGenerico : IRepository<CatalogoGenerico>
    {
        IEnumerable<BoCatalogoGenerico> All(string tipoDeCatalogo);
        int InsertOrUpdate(BoCatalogoGenerico catalogoGenerico);
        Task<bool> Delete(int idCatalogoGenerico);
    }
}
