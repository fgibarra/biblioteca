﻿using System.Collections.Generic;
using Ksp.Biblioteca.Bo;

namespace Ksp.Biblioteca.Bll.Core
{
    public interface IBibliotecaService
    {
        IEnumerable<BoAutor> GetAllAutores();        
        int InsertOrUpdateAutor(BoAutor autor);
        void DeleteAutor(int idAutor);

        IEnumerable<BoLibro> GetAllLibros();
        int InsertOrUpdateLibro(BoLibro libro);
        void DeleteLibro(int idLibro);

    }
}
