﻿using System.ComponentModel.DataAnnotations;

namespace Ksp.Biblioteca.Bo
{
    public class BoLibro
    {
        [Key]
        public int IdLibro { get; set; }

        
        public string Titulo { get; set; }
        
        public string Editorial { get; set; }

        [Display(Name = "Año")]
        public int Anio { get; set; }

        [Display(AutoGenerateField = false)]
        public int IdTipoDeLibro { get; set; }

        [Display(AutoGenerateField = false)]
        public int IdAutor { get; set; }

        [Display(AutoGenerateField = false)]
        public virtual BoAutor Autor { get; set; }

    }
}
