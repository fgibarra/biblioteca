﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ksp.Biblioteca.Dal.Entities
{
    [Table("CatalogoGenerico")]
    public class CatalogoGenerico
    {
        [Key]
        public int IdCatalogoGenerico { get; set; }

        
        public string Clave { get; set; }
        
        public string Descripcion { get; set; }

        
        public string TipoDeCatalogo { get; set; }
    }
}
