﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ksp.Biblioteca.Dal.Entities
{
    [Table("Autor")]
    public class Autor
    {
        public Autor()
        {
            Libros = new HashSet<Libro>();
        }

        [Key]
        public int IdAutor { get; set; }

        [StringLength(150)]
        public string Nombre { get; set; }

        [StringLength(50)]
        public string Nacionalidad { get; set; }

        //[Column(TypeName = "real")]
        public string FechaDeNacimiento { get; set; }

        public virtual ICollection<Libro> Libros { get; set; }
    }
}
