﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ksp.Biblioteca.Dal.Core
{
    public  interface IUnitOfBiblioteca : IDisposable
    {
        IRepositoryAutores Autores { get; }
        IRepositoryLibros Libros { get; }

        int Complete();
    }
  
}
