﻿using System;

namespace Ksp.Biblioteca.Bo.Helpers
{
    // Revisar http://stackoverflow.com/questions/6145888/how-to-bind-an-enum-to-a-combobox-control-in-wpf
    //La solución es bastante elegante
    /// <summary>
    /// Permite almacenar una enumeracion y su descripción.
    /// </summary>
    public class ValueDescription
    {
        public Enum Value { get; set; }
        public string Description { get; set; }
    }
}
