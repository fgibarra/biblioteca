﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ksp.Biblioteca.Dal.Core;
using Ksp.Biblioteca.Dal.Database;
using Ksp.Biblioteca.Dal.Repositories;

namespace Ksp.Biblioteca.Dal.UnitsOfWork
{
    public class UnitOfCatalogos : IUnitOfCatalogos
    {
        private readonly BibliotecaContext _bibliotecaContext;


        public UnitOfCatalogos(BibliotecaContext bibliotecaContext)
        {
            _bibliotecaContext = bibliotecaContext;
            CatalogosGenericos = new RepositoryCatalogoGenerico(_bibliotecaContext);
        }

        public void Dispose()
        {
           _bibliotecaContext.Dispose();
        }

        public IRepositoryCatalogoGenerico CatalogosGenericos { get; private set; }
        public int Complete()
        {
            return _bibliotecaContext.SaveChanges();
        }
    }
}
