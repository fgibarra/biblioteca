﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Ksp.Biblioteca.Bo;
using Ksp.Biblioteca.Dal.Core;
using Ksp.Biblioteca.Dal.Database;
using Ksp.Biblioteca.Dal.Entities;

namespace Ksp.Biblioteca.Dal.Repositories
{
    public class RepositoryAutores :Repository<Autor>, IRepositoryAutores
    {
        public RepositoryAutores(BibliotecaContext context) : base(context)
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Autor, BoAutor>();
                cfg.CreateMap<BoAutor, Autor>();
            });
        }

        public IEnumerable<BoAutor> All()
        {
            try
            {
                var entities = Queryable().ToList();

                return Mapper
                      .Map<IEnumerable<Autor>, IEnumerable<BoAutor>>(entities);
            }
            catch (Exception)
            {
                return new List<BoAutor>();
            }

        }

        public int InsertOrUpdate(BoAutor autor)
        {
            var entidad = Mapper
                .Map<BoAutor, Autor>(autor);

            if (entidad.IdAutor == 0)
            {
                //Verificando si existe el autor
                var autorExistente = Queryable().SingleOrDefault(a => a.Nombre.Contains(autor.Nombre));

                if (autorExistente != null)
                {
                    Update(entidad);
                }
                else
                {
                    Add(entidad);
                }               
            }
            else
            {
                Update(entidad);
            }

            return entidad.IdAutor;
        }

        public async Task<bool> Delete(int idAutor)
        {
            await Remove(idAutor);

            return true;
        }
    }
}
