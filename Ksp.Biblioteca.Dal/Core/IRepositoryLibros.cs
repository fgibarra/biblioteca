﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Ksp.Biblioteca.Bo;
using Ksp.Biblioteca.Dal.Entities;

namespace Ksp.Biblioteca.Dal.Core
{
    public interface IRepositoryLibros : IRepository<Libro>
    {
        IEnumerable<BoLibro> All();
        int InsertOrUpdate(BoLibro libro);
        Task<bool> Delete(int idLibro);
    }
}
