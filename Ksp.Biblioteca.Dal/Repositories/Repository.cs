﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Ksp.Biblioteca.Dal.Core;
using Ksp.Biblioteca.Dal.Database;

namespace Ksp.Biblioteca.Dal.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> 
        where TEntity : class        
    {
        protected readonly BibliotecaContext Context;
        protected readonly DbSet<TEntity> DbSet;

        public Repository(BibliotecaContext context)
        {
            this.Context = context;
            this.DbSet = context.Set<TEntity>();
        }

        public TEntity Get(int id)
        {
            return Context.Set<TEntity>().Find(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return Context.Set<TEntity>().ToList();
        }

        public void Add(TEntity entity)
        {
            DbSet.Add(entity);
        }
        public void AddRange(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                Add(entity);
            }
        }


        public void Remove(TEntity entity)
        {
            if (Context.Entry(entity).State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }
            DbSet.Remove(entity);
        }
        public async Task Remove(params object[] id)
        {
            TEntity entity = await this.FindAsync(id);
            if (entity != null)
            {
                this.Remove(entity);
            }
        }


        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            Context.Set<TEntity>().RemoveRange(entities);
        }


        public void Update(TEntity entity)
        {
            DbSet.Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
        }

        public async Task<TEntity> FindAsync(params object[] keyValues)
        {
            return await DbSet.FindAsync(keyValues);
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
           return Context.Set<TEntity>().Where(predicate);           
        }

        public TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return Context.Set<TEntity>().SingleOrDefault(predicate);
        }

        public virtual IQueryable<TEntity> SelectQuery(string query, params object[] parameters)
        {
            return DbSet.SqlQuery(query, parameters).AsQueryable();
        }


        public IQueryable<TEntity> Queryable()
        {
            return DbSet;
        }


        //public IEnumerable<TBusiness> Find(Expression<Func<TEntity, bool>> predicate)
        //{
        //    Mapper.Initialize(cfg => {
        //        cfg.CreateMap<TEntity, TBusiness>();
        //    });

        //    var founded = Context.Set<TEntity>().Where(predicate);

        //    var result = Mapper
        //        .Map<IEnumerable<TEntity>, IEnumerable<TBusiness>>(founded);

        //    return result;
        //}


        //public TBusiness Get(int id)
        //{
        //    var entity =  Context.Set<TEntity>().Find(id);

        //    Mapper.Initialize(cfg => {
        //        cfg.CreateMap<TEntity, TBusiness>();
        //    });

        //    return Mapper.Map<TEntity, TBusiness>(entity);
        //}

        //public IEnumerable<TBusiness> GetAll()
        //{
        //    Mapper.Initialize(cfg => {
        //        cfg.CreateMap<TEntity, TBusiness>();
        //    });

        //    var founded = Context.Set<TEntity>().ToList();

        //    var result = Mapper
        //        .Map<IEnumerable<TEntity>, IEnumerable<TBusiness>>(founded);

        //    return result;
        //}

        //public int InsertOrUpdate(TBusiness business)
        //{
            
        //}

        //public void DeleteRange(IEnumerable<TBusiness> business)
        //{
        //    Mapper.Initialize(cfg => {
        //        cfg.CreateMap<TBusiness, TEntity>();
        //    });

        //    var forDelete = Mapper
        //        .Map<IEnumerable<TBusiness>, IEnumerable<TEntity>>(business);

        //    RemoveRange(forDelete);
        //}

        //public void Delete(TBusiness entity)
        //{
        //    throw new NotImplementedException();
        //}

        //public Task Delete(params object[] id)
        //{
        //    throw new NotImplementedException();
        //}


        //public TBusiness SingleOrDefault(Expression<Func<TEntity, bool>> predicate)
        //{
        //    return Context.Set<TEntity>().SingleOrDefault(predicate);
        //}


    }
}
