﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ksp.Biblioteca.Bo
{
    public enum TipoDeCatalogo
    {
        [Description("Tipo de Libro")]
        TipoDeLibro,
        [Description("Estado de la Copia")]
        EstadoDeLaCopia      
    }
}
