﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ksp.Biblioteca.Bo
{
    public class BoAutor
    {
        public BoAutor()
        {
            Libros = new List<BoLibro>();
        }

        [Key]
        [Display(AutoGenerateField = false)]
        public int IdAutor { get; set; }

        
        public string Nombre { get; set; }

        
        public string Nacionalidad { get; set; }

        
        [Display(Name = "Fecha de Nacimiento")]
        public string FechaDeNacimiento { get; set; }

        public virtual ICollection<BoLibro> Libros { get; set; }
    }
}
