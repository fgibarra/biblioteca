﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Ksp.Biblioteca.Bo;
using Ksp.Biblioteca.Dal.Entities;

namespace Ksp.Biblioteca.Dal.Core
{
    public interface IRepositoryAutores : IRepository<Autor>
    {
        IEnumerable<BoAutor> All();
        int InsertOrUpdate(BoAutor autor);
        Task<bool> Delete(int idAutor);
    }
}
