CREATE TABLE IF NOT EXISTS [CatalogoGenerico] (
	[IdCatalogoGenerico] integer NOT NULL PRIMARY KEY AUTOINCREMENT, 
	[Clave] varchar(20), 
	[Descripcion] varchar(100),
	[TipoDeCatalogo] varchar(25)
)