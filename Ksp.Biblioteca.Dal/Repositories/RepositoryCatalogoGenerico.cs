﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Ksp.Biblioteca.Bo;
using Ksp.Biblioteca.Dal.Core;
using Ksp.Biblioteca.Dal.Database;
using Ksp.Biblioteca.Dal.Entities;

namespace Ksp.Biblioteca.Dal.Repositories
{
    public class RepositoryCatalogoGenerico: Repository<CatalogoGenerico>, IRepositoryCatalogoGenerico
    {
        public RepositoryCatalogoGenerico(BibliotecaContext context) : base(context)
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<CatalogoGenerico, BoCatalogoGenerico>();
                cfg.CreateMap<BoCatalogoGenerico, CatalogoGenerico>();
            });
        }

        public IEnumerable<BoCatalogoGenerico> All(string tipoDeCatalogo)
        {
            try
            {
                var entities = Queryable().Where(c => c.TipoDeCatalogo == tipoDeCatalogo).ToList();

                return Mapper
                      .Map<IEnumerable<CatalogoGenerico>, IEnumerable<BoCatalogoGenerico>>(entities);
            }
            catch (Exception)
            {
                return new List<BoCatalogoGenerico>();
            }
            
        }

        public int InsertOrUpdate(BoCatalogoGenerico catalogoGenerico)
        {
            var entidad = Mapper
                .Map<BoCatalogoGenerico, CatalogoGenerico>(catalogoGenerico);
            
            if (entidad.IdCatalogoGenerico == 0)
            {
                Add(entidad);
                
            }
            else
            {
                Update(entidad);
               
            }

            return entidad.IdCatalogoGenerico;
        }

        public async Task<bool> Delete(int idCatalogoGenerico)
        {
           await Remove(idCatalogoGenerico);

            return true;
        }
    }
}
