﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ksp.Biblioteca.Dal.Entities;

namespace Ksp.Biblioteca.Dal.Database
{
    public class BibliotecaContext: DbContext
    {
        public BibliotecaContext() : base("name=BibliotecaContext")
        {
           System.Data.Entity.Database.SetInitializer(new BibliotecaInitializer());
           Database.Initialize(false);
            
        }

        

        public DbSet<CatalogoGenerico> CatalogoGenericos { get; set; }
        public DbSet<Autor> Autores { get; set; }

        public DbSet<Libro> Libros { get; set; }


    }
}
