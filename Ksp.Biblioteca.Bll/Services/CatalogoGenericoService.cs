﻿using System.Collections.Generic;
using Ksp.Biblioteca.Bll.Core;
using Ksp.Biblioteca.Bll.Helpers;
using Ksp.Biblioteca.Bo;
using Ksp.Biblioteca.Bo.Helpers;
using Ksp.Biblioteca.Dal.Core;

namespace Ksp.Biblioteca.Bll.Services
{
    public class CatalogoGenericoService : ICatalogoGenericoService
    {
        private readonly IUnitOfCatalogos _unitCatalogos;

        public CatalogoGenericoService(IUnitOfCatalogos unitCatalogos)
        {
            _unitCatalogos = unitCatalogos;
        }


        public IEnumerable<BoCatalogoGenerico> GetAll(string tipoDeCatalogo) => 
            _unitCatalogos.CatalogosGenericos.All(tipoDeCatalogo);


        public IEnumerable<ValueDescription> GetTiposDeCatalogo() => 
            EnumHelper.GetAllValuesAndDescriptions<TipoDeCatalogo>();

        public void InsertOrUpdateItemCatalogoGenerico(BoCatalogoGenerico item)
        {
            _unitCatalogos.CatalogosGenericos.InsertOrUpdate(item);
            _unitCatalogos.Complete();
        }

        public void DeleteItemCatalogoGenerico(int idCatalogoGenerico)
        {
             _unitCatalogos.CatalogosGenericos.Delete(idCatalogoGenerico);
            _unitCatalogos.Complete();
        }
    }
}
