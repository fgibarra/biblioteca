﻿using System.Data.Entity;
using Ksp.Biblioteca.Dal.Entities;
using Ksp.Biblioteca.Dal.Properties;

namespace Ksp.Biblioteca.Dal.Database
{
    public class BibliotecaInitializerCreate: CreateDatabaseIfNotExists<BibliotecaContext>
    {
        protected override void Seed(BibliotecaContext context)
        {
            
            context.Database.ExecuteSqlCommand(Resources.CrearTablas);
            

            //Catalogo Tipo de libro
            context.CatalogoGenericos.Add(new CatalogoGenerico
            {
                Clave = "Teatro",
                Descripcion = "Genero Literario Teatro",
                TipoDeCatalogo = "TipoDeLibro"
            });

            context.CatalogoGenericos.Add(new CatalogoGenerico
            {
                Clave = "Novela",
                Descripcion = "Genero Literario Novela",
                TipoDeCatalogo = "TipoDeLibro"
            });

            context.CatalogoGenericos.Add(new CatalogoGenerico
            {
                Clave = "Ensayo",
                Descripcion = "Genero Literario Ensayo",
                TipoDeCatalogo = "TipoDeLibro"
            });

            context.CatalogoGenericos.Add(new CatalogoGenerico
            {
                Clave = "Poesia",
                Descripcion = "Genero Literario Poesia",
                TipoDeCatalogo = "TipoDeLibro"
            });

            //Catalogo Estado de Copia
            context.CatalogoGenericos.Add(new CatalogoGenerico
            {
                Clave = "Prestado",
                Descripcion = "Libro Prestado a un Lector.",
                TipoDeCatalogo = "EstadoDeLaCopia"
            });

            context.CatalogoGenericos.Add(new CatalogoGenerico
            {
                Clave = "Retraso",
                Descripcion = "El Lector esta retrasado en la entrega de este libro.",
                TipoDeCatalogo = "EstadoDeLaCopia"
            });

            context.CatalogoGenericos.Add(new CatalogoGenerico
            {
                Clave = "Biblioteca",
                Descripcion = "El libro esta en la biblioteca.",
                TipoDeCatalogo = "EstadoDeLaCopia"
            });

            context.CatalogoGenericos.Add(new CatalogoGenerico
            {
                Clave = "Reparación",
                Descripcion = "El libro esta en reparación.",
                TipoDeCatalogo = "EstadoDeLaCopia"
            });

            context.SaveChanges();
            base.Seed(context);
        }
    }
}
