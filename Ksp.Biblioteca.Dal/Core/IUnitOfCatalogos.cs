﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ksp.Biblioteca.Dal.Core
{
    public interface IUnitOfCatalogos : IDisposable
    {
        IRepositoryCatalogoGenerico CatalogosGenericos { get; }
       
        int Complete();
    }
}
