using System.Web;
using System.Web.Mvc;

namespace Ksp_Biblioteca_Web_DX {
    public class FilterConfig {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters) {
            filters.Add(new HandleErrorAttribute());
        }
    }
}