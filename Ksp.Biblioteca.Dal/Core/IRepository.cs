﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Ksp.Biblioteca.Dal.Core
{
    //public interface IRepository<TBusiness,TEntity> 
    //    where TBusiness : class 
    //    where TEntity:class
    public interface IRepository<TEntity>    
    where TEntity : class
    {
        //Solo para entidades
        TEntity Get(int id);
        IEnumerable<TEntity> GetAll();

        void Add(TEntity entity);
        void AddRange(IEnumerable<TEntity> entities);

        void RemoveRange(IEnumerable<TEntity> entities);

        void Remove(TEntity entity);

        Task Remove(params object[] id);

        void Update(TEntity entity);

        Task<TEntity> FindAsync(params object[] keyValues);

        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);

        IQueryable<TEntity> SelectQuery(string query, params object[] parameters);

        TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate);

        IQueryable<TEntity> Queryable();

        ////Solo para negocios


        //TBusiness Get(int id);
        //IEnumerable<TBusiness> GetAll();

        //int InsertOrUpdate(TBusiness business);

        //void DeleteRange(IEnumerable<TBusiness> entities);

        //void Delete(TBusiness entity);

        //Task Delete(params object[] id);
    }

}
