﻿using System.Collections.Generic;

namespace Ksp.Biblioteca.Bo
{
    public class BoBiblioteca
    {
        public IEnumerable<BoLibro> Libros { get; set; }
        public IEnumerable<BoAutor> Autores { get; set; }

        public IEnumerable<BoCatalogoGenerico> TiposDeLibros { get; set; }

        public IEnumerable<BoCatalogoGenerico> EstadosDeLaCopia { get; set; }


        public BoBiblioteca()
        {
            Libros = new List<BoLibro>();
            Autores = new List<BoAutor>();
            TiposDeLibros= new List<BoCatalogoGenerico>();
            EstadosDeLaCopia = new List<BoCatalogoGenerico>();
        }
    }
}
