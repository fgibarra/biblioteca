﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ksp.Biblioteca.Dal.Entities
{
    [Table("Libro")]
    public class Libro
    {
        [Key]
        public int IdLibro { get; set; }

        [StringLength(50)]
        public string Titulo { get; set; }

        [StringLength(50)]
        public string Editorial { get; set; }

        
        public int Anio { get; set; }

        public int IdTipoDeLibro { get; set; }

        public int IdAutor { get; set; }

        public virtual Autor Autor { get; set; }
    }
}
