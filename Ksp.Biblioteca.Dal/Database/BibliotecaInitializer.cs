﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ksp.Biblioteca.Dal.Entities;
using Ksp.Biblioteca.Dal.Properties;

namespace Ksp.Biblioteca.Dal.Database
{
    class BibliotecaInitializer :IDatabaseInitializer<BibliotecaContext>
    {
        
        

        public void InitializeDatabase(BibliotecaContext context)
        {
            

            context.Database.ExecuteSqlCommand(Resources.CrearTablas);
            
            if (!context.CatalogoGenericos.ToList().Any())
            {
                //Catalogo Tipo de libro
                context.CatalogoGenericos.Add(new CatalogoGenerico
                {
                    Clave = "Teatro",
                    Descripcion = "Genero Literario Teatro",
                    TipoDeCatalogo = "TipoDeLibro"
                });

                context.CatalogoGenericos.Add(new CatalogoGenerico
                {
                    Clave = "Novela",
                    Descripcion = "Genero Literario Novela",
                    TipoDeCatalogo = "TipoDeLibro"
                });

                context.CatalogoGenericos.Add(new CatalogoGenerico
                {
                    Clave = "Ensayo",
                    Descripcion = "Genero Literario Ensayo",
                    TipoDeCatalogo = "TipoDeLibro"
                });

                context.CatalogoGenericos.Add(new CatalogoGenerico
                {
                    Clave = "Poesia",
                    Descripcion = "Genero Literario Poesia",
                    TipoDeCatalogo = "TipoDeLibro"
                });

                //Catalogo Estado de Copia
                context.CatalogoGenericos.Add(new CatalogoGenerico
                {
                    Clave = "Prestado",
                    Descripcion = "Libro Prestado a un Lector.",
                    TipoDeCatalogo = "EstadoDeLaCopia"
                });

                context.CatalogoGenericos.Add(new CatalogoGenerico
                {
                    Clave = "Retraso",
                    Descripcion = "El Lector esta retrasado en la entrega de este libro.",
                    TipoDeCatalogo = "EstadoDeLaCopia"
                });

                context.CatalogoGenericos.Add(new CatalogoGenerico
                {
                    Clave = "Biblioteca",
                    Descripcion = "El libro esta en la biblioteca.",
                    TipoDeCatalogo = "EstadoDeLaCopia"
                });

                context.CatalogoGenericos.Add(new CatalogoGenerico
                {
                    Clave = "Reparación",
                    Descripcion = "El libro esta en reparación.",
                    TipoDeCatalogo = "EstadoDeLaCopia"
                });

                context.SaveChanges();
            }
           
        }
    }
}
