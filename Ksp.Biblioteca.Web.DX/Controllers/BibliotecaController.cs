﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ksp.Biblioteca.Bll.Core;
using Ksp.Biblioteca.Bo;

namespace Ksp.Biblioteca.Web.DX.Controllers
{
    public class BibliotecaController : Controller
    {
        private readonly ICatalogoGenericoService _catalogoService;
        private readonly IBibliotecaService _bibliotecaService;

        public BibliotecaController(ICatalogoGenericoService catalogoService, 
            IBibliotecaService bibliotecaService)
        {
            _catalogoService = catalogoService;
            _bibliotecaService = bibliotecaService;
        }

        // GET: Biblioteca
        public ActionResult Index()
        {
            return View(GetBiblioteca());
        }



        #region Libros

        public ActionResult LibrosPartial(string tipo)
        {
            return PartialView("GridViewLibrosPartial", GetBiblioteca());
        }

        [HttpPost]
        public ActionResult AddOrUpdateLibroPartial(         
            BoLibro item)
        {
            if (ModelState.IsValid)
            {
                try
                {
                   
                    _bibliotecaService.InsertOrUpdateLibro(item);
                }
                catch (Exception ex)
                {
                    ViewData["Errores"] = ex.Message;
                    ViewData["item"] = item;
                }
            }
            else
            {
                ViewData["Errores"] = "Corrije los errores.";
                ViewData["item"] = item;
            }

            return PartialView("GridViewLibrosPartial", GetBiblioteca());
        }


        [HttpPost]
        public ActionResult DeletePartial(int idLibro)
        {
            if (idLibro > 0)
            {
                try
                {
                    _bibliotecaService.DeleteLibro(idLibro);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("GridViewLibrosPartial", GetBiblioteca().Libros);
        }

        #endregion

        public BoBiblioteca GetBiblioteca()
        {
            var biblioteca = new BoBiblioteca();

            biblioteca.Libros =  _bibliotecaService.GetAllLibros();
            biblioteca.Autores = _bibliotecaService.GetAllAutores();

            //TODO: Cambiar enumeracion a Tostring
            biblioteca.TiposDeLibros = _catalogoService.GetAll("TipoDeLibro");
            biblioteca.EstadosDeLaCopia = _catalogoService.GetAll("EstadoDeLaCopia");

            return biblioteca;
        }
    }
}