﻿using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using Ksp.Biblioteca.Bll.Core;
using Ksp.Biblioteca.Bo;
using Ksp.Biblioteca.Dal.Core;

namespace Ksp.Biblioteca.Bll.Services
{
    public class BibliotecaService : IBibliotecaService
    {
        private readonly IUnitOfBiblioteca _unitBiblioteca;

        public BibliotecaService(IUnitOfBiblioteca unitBiblioteca)
        {
            _unitBiblioteca = unitBiblioteca;
        }

        public IEnumerable<BoAutor> GetAllAutores() => _unitBiblioteca.Autores.All();

        public int InsertOrUpdateAutor(BoAutor autor)
        {
           return   _unitBiblioteca.Autores.InsertOrUpdate(autor);           
        }

        public void DeleteAutor(int idAutor)
        {
            //Eliminar las copias de los libros del autor

            //Eliminar los libros del autor


            _unitBiblioteca.Autores.Delete(idAutor);

            //_unitBiblioteca.Complete();
        }

        public IEnumerable<BoLibro> GetAllLibros() => _unitBiblioteca.Libros.All();
        

        public int InsertOrUpdateLibro(BoLibro libro)
        {
            int idAutor = _unitBiblioteca.Autores.InsertOrUpdate(libro.Autor);
            libro.IdAutor = idAutor;
            int idLibro = _unitBiblioteca.Libros.InsertOrUpdate(libro);

            _unitBiblioteca.Complete();

            return idLibro;
        }

        public void DeleteLibro(int idLibro)
        {
            throw new System.NotImplementedException();
        }
    }
}
