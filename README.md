## Objetivo

	El objetivo de este código es mostrar la arquitectura, patrones que se estan usando con MCV.

## Resumen del proyecto

	Una biblioteca tiene copias de libros. Estos últimos se caracterizan por su nombre, tipo (novela, teatro, poesia, ensayo), editorial, año y autor.
    Los autores se caracterizan por su nombre, nacionalidad y fecha de nacimiento.
    Cada copia tiene un identificador, y puede estar en la bibilioteca, prestada, con retraso o en reparación.
    Los lectores pueden tener un máximo de 3 libros en préstamo.
    Cada libro se presta en un máximo de 30 dias, por cada dia de retraso, 
	se impone una "multa" de dos dias sin posibilidad de coger un nuevo libro.
	
## Caracteristicas del proyecto

	1. Visual Studio 2015	
	2. Multicapa (Dal, Bo, Bll)
	3. C#, MVC5
	4. Ninject (Inyección de Dependencias)
	5. DevExpress 16.1
	6. Patrones usados
		*Repository
		*UnitOfWork
	7.Sqlite

## Consideraciones

	El código aun no esta completo, faltan muchos detalles que afinar y su objetivo es ser demostrativo totalmente.
	
		


