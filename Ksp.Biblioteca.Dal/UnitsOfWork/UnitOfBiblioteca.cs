﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ksp.Biblioteca.Dal.Core;
using Ksp.Biblioteca.Dal.Database;
using Ksp.Biblioteca.Dal.Repositories;

namespace Ksp.Biblioteca.Dal.UnitsOfWork
{
    public class UnitOfBiblioteca : IUnitOfBiblioteca
    {
        private readonly BibliotecaContext _bibliotecaContext;

    
        public UnitOfBiblioteca(BibliotecaContext bibliotecaContext)
        {
            _bibliotecaContext = bibliotecaContext;
            Autores = new RepositoryAutores(_bibliotecaContext);
            Libros = new RepositoryLibros(_bibliotecaContext);
        }

        public void Dispose()
        {
            _bibliotecaContext.Dispose();
        }

        public IRepositoryAutores Autores { get; }
        public IRepositoryLibros Libros { get; }

        public int Complete()
        {
            return _bibliotecaContext.SaveChanges();
        }
    }
}
