﻿using System.ComponentModel.DataAnnotations;

namespace Ksp.Biblioteca.Bo
{

    public class BoCatalogoGenerico
    {
        [Key]
        public int IdCatalogoGenerico { get; set; }

        [Display(Name = "Clave")]
        public string Clave { get; set; }

        [Display(Name = "Descripción")]
        public string Descripcion { get; set; }

        [Display(Name = "Tipo de Catálogo")]
        public string TipoDeCatalogo { get; set; }
    }
}
