﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ksp.Biblioteca.Bll;
using Ksp.Biblioteca.Bll.Core;
using Ksp.Biblioteca.Bo;

namespace Ksp.Biblioteca.Web.UI.Controllers
{
    public class CatalogoGenericoController : Controller
    {
        private readonly ICatalogoGenericoService _service;
        // GET: CatalogoGenerico

        



        public CatalogoGenericoController(ICatalogoGenericoService service)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            return View("Index", GetItems());
        }

        public ActionResult IndexPartial()
        {
            return PartialView("IndexPartial", GetItems());
        }


        public IEnumerable<BoCatalogoGenerico> GetItems()
        {
            return _service.GetAll("Todos");
        } 

    }
}