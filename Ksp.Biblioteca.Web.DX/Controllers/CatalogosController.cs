﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Ksp.Biblioteca.Bll.Core;
using Ksp.Biblioteca.Bo;
using Ksp.Biblioteca.Bo.Helpers;

namespace Ksp.Biblioteca.Web.DX.Controllers
{
    public class CatalogosController : Controller
    {
        private readonly ICatalogoGenericoService _service;
        private readonly IEnumerable<ValueDescription> _tiposDeCatalogo;
        // GET: CatalogoGenerico

        public CatalogosController(ICatalogoGenericoService service)
        {
            _service = service;
            _tiposDeCatalogo = _service.GetTiposDeCatalogo();
        }

        public ActionResult Index()
        {   
            //Definiendo los datos a usar en las estruturas parciales
            string tipo = _tiposDeCatalogo.First().Value.ToString();
            ViewData["TiposDeCatalogo"] = _tiposDeCatalogo;
            ViewData["Tipo"] = tipo;

            return View("Index", GetItems(tipo));
        }

        public ActionResult GridViewCatalogosPartial(string tipo)
        {            
            return PartialView("GridViewCatalogosPartial", GetItems(tipo));
        }

        [HttpPost]  
        public ActionResult AddOrUpdatePartial(
            [Bind(Include = "IdCatalogoGenerico,Clave,Descripcion,TipoDeCatalogo, tipo")]
            BoCatalogoGenerico item, string tipo)
        {
            if (ModelState.IsValid)
            { 
                try
                {
                    item.TipoDeCatalogo = tipo;
                    _service.InsertOrUpdateItemCatalogoGenerico(item);
                }
                catch (Exception ex)
                {
                    ViewData["EditError"] = ex.Message;
                    ViewData["item"] = item;
                }                
            }
            else
            {
                ViewData["EditError"] = "Corrije los errores.";
                ViewData["item"] = item;
            }

            return PartialView("GridViewCatalogosPartial", GetItems(tipo));
        }


        [HttpPost]
        public ActionResult DeletePartial(int idCatalogoGenerico, string tipo)
        {
            if (idCatalogoGenerico > 0)
            {
                try
                {
                    _service.DeleteItemCatalogoGenerico(idCatalogoGenerico);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("GridViewCatalogosPartial",GetItems(tipo));
        }

        public IEnumerable<BoCatalogoGenerico> GetItems(string tipo)
        {
            return _service.GetAll(tipo);
        }
    }
}